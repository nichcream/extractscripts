#!/bin/bash
#
# Copyright (C) 2016 The CyanogenMod Project
# Copyright (C) 2017 The LineageOS Project
#           (C) 2017 The CMrience Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

set -e

DEVICE=land
VENDOR=xiaomi

# Load extractutils and do some sanity checks
MY_DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$MY_DIR" ]]; then MY_DIR="$PWD"; fi

LINEAGE_ROOT="$MY_DIR"

HELPER="$LINEAGE_ROOT"/tools/extract_utils.sh
if [ ! -f "$HELPER" ]; then
    echo "Unable to find helper script at $HELPER"
    exit 1
fi
. "$HELPER"

if [ $# -ge 1 ]; then
    SRC=$1
    if [ -n "$2" ]; then
      LIST=$2
    fi
  else
    echo "$0: bad number of arguments"
    echo ""
    echo "usage: $0 [PATH_TO_EXPANDED_ROM] [PATH_TO_PROPRIETARY_FILES_LIST (optional)]"
    echo ""
    echo "If PATH_TO_EXPANDED_ROM is not specified, blobs will be extracted from"
    echo "the device using adb pull."
    echo "If PATH_TO_PROPRIETARY_FILES_LIST is not specified, blobs will be"
    echo "extracted based on $MY_DIR/files.txt."
    exit 1
fi

# Initialize the helper
setup_vendor "$DEVICE" "$VENDOR" "$LINEAGE_ROOT"

if [ -n "$LIST" ]; then
    extract "$LIST" "$SRC"
  else
    echo "Defaulting to ./files.txt..."
    extract "$MY_DIR"/files.txt "$SRC"
fi

#"$MY_DIR"/setup-makefiles.sh
